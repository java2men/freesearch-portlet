<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="false"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<portlet:defineObjects />
<%@page import="javax.portlet.WindowState"%>
<%@ page import="ru.admomsk.server.beans.ViewBean" %>
<jsp:useBean id="viewBean" class="ru.admomsk.server.beans.ViewBean" scope="request"/>

<c:if test="${not viewBean.editBean.validAll}">
	<div class="journal-content-article">
		<h2>Портлет не сконфигурирован!</h2>
		<div class="form-error-message">В настройках портлета имеются ошибки, исправьте их, пожалуйста!</div>
	</div>
</c:if>

<c:if test="${viewBean.editBean.validAll}">

	<div id="kindergarden" >
		
		<form accept-charset="utf-8" id="freesearch_form" name="freesearch_form" method="post" action="<portlet:renderURL></portlet:renderURL>&np=0" class="jotform-form">
			<div class="form-all">
				<ul class="form-section">
					<li class="form-line">
						<div class="form-input-wide">
							<span class="form-sub-label-container">
								<input id="text_query" name="q" type="text" size="50" value="${viewBean.textQuery}" >
		      					<input type="hidden" value="Search!" name="cmd">
								<button id="freesearch_send" class="form-submit-button" type="submit">Найти</button>
							</span>
						</div>
					</li>
				</ul>
			</div>
		</form>
		
	</div>
		
	
	<div id="result_query">
		<%=((ViewBean)renderRequest.getPortletSession().getAttribute("viewBean")).getResultQuery()%>
	</div>

</c:if>