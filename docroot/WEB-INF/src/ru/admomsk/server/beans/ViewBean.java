package ru.admomsk.server.beans;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

public class ViewBean implements Serializable {
	
	private static final long serialVersionUID = -2659757676334276661L;
	private static Log _log = LogFactoryUtil.getLog("FreeSearch-portlet, class ViewBean");
	private String resultQuery = "";
	private String textQuery = "";		
	
	private EditBean editBean;
	
	public ViewBean() {
		
	}
	
	public String getResultQuery() {
		return resultQuery;
	}
	public void setResultQuery(String resultQuery) {
		if (resultQuery == null) {
			this.resultQuery = "";
		}
		else {
			this.resultQuery = resultQuery;
		}
	}
	
	public String getTextQuery() {
		return textQuery;
	}
	public void setTextQuery(String textQuery) {
		this.textQuery = textQuery;
	}
	
	public EditBean getEditBean() {
		return editBean;
	}
	public void setEditBean(EditBean editBean) {
		this.editBean = editBean;
	}
	
	public void resetValue() {
		textQuery = "";
		resultQuery = "";
		
	}
	
	public String connectMnoGoSearch(String parameter, String replace) {
		URL url;
		BufferedReader br = null;
		StringBuffer sbResultString = null;
		try {
			//заменить в строке параметров пробелы на +
            url = new URL(editBean.getHost() + "?" + parameter.replace(" ", "+"));
            br = new BufferedReader(new InputStreamReader(url.openStream()));
            String line;
            sbResultString = new StringBuffer();
            while ((line = br.readLine()) != null) {
            	sbResultString.append(line);
            }
            
		} catch (Exception e) {
			_log.error(e);
		} finally {
	        try {
	        	if (br != null) br.close();
			} catch (Exception e) {
				_log.error(e);
			}
		} 
        
		String resultString;
		
		if (sbResultString == null) {
			resultString = "<p><b>Удаленный сервер не отвечает, пожалуйста, повторите попытку позже!</b></p>";
			return resultString;
		} else {
			if (sbResultString.length() == 0) {
				resultString = "<p><b>Удаленный сервер не отвечает, пожалуйста, повторите попытку позже!</b></p>";
				return resultString;
			}
			resultString = sbResultString.toString();
		}
        
        String newHost;
        URL newURL;
        try {
        	newURL = new URL(replace);
        	newHost = newURL.getHost();
        	if (newURL.getPort() != -1){
        		newHost += ":" + String.valueOf(newURL.getPort());
        	}
        	if (editBean.getHostSearch() != null) {
        		if (!editBean.getHostSearch().equals("") && !editBean.getHostSearch().equals(newHost)) {
        			resultString = resultString.replace(editBean.getHostSearch(), newHost);
        		}
        	}
        	
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        //заменить ссылки
		resultString = resultString.replace("<a href=\"?cmd=", "<a href=\""+ replace +"&cmd=");
        resultString = resultString.replace("<A HREF=\"?cmd=", "<a href=\""+ replace +"&cmd=");
        
        //удалить строку поиска
        int startIndex = resultString.indexOf("<div class=\"center\">");
        if (startIndex != -1) {
        	String removeSubString = resultString.substring(startIndex, resultString.indexOf("</div>", startIndex) + "</div>".length());
            //System.out.println("removeSubString = " + removeSubString);
            resultString = resultString.replace(removeSubString, "");
        }
        return resultString;
	}
}
