package ru.admomsk.server.beans;

import java.io.IOException;
import java.io.Serializable;

import javax.portlet.PortletPreferences;
import javax.portlet.ReadOnlyException;
import javax.portlet.ValidatorException;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

public class EditBean implements Serializable {
	
	private static final long serialVersionUID = -2014204537198554574L;
	private static Log _log = LogFactoryUtil.getLog("FreeSearch-portlet, class EditBean");
	
	private PortletPreferences portletPreferences;
	
    private String host = "";
    private String hostSearch = "";
    private String validHost = "";
	private String idle = "idle";
	private String ok = "ok";
	private boolean validAll;
	
    public PortletPreferences getPortletPreferences() {
		return portletPreferences;
	}
	public void setPortletPreferences(PortletPreferences portletPreferences) {
		this.portletPreferences = portletPreferences;
	}
		
	public String getHost() {
		host = (String)portletPreferences.getValue("host", "");
		return host;
	}
	public void setHost(String host) throws ReadOnlyException, ValidatorException, IOException {
		if (host == null) {
			host = "";
		}
		this.host = host;
		portletPreferences.setValue("host", this.host);
		portletPreferences.store();
	}
	
	public String getHostSearch() {
		hostSearch = (String)portletPreferences.getValue("hostSearch", "");
		return hostSearch;
	}
	public void setHostSearch(String hostSearch) throws ReadOnlyException, ValidatorException, IOException {
		if (hostSearch == null) {
			hostSearch = "";
		}
		this.hostSearch = hostSearch;
		portletPreferences.setValue("hostSearch", this.hostSearch);
		portletPreferences.store();
	}
	
	public String getValidHost() {
		return validHost;
	}
	public void validateHost() {
		if (host.equals("")) validHost = idle;
		else validHost = ok;
	}
	
	public String getIdle() {
		return idle;
	}
	public void setIdle(String idle) {
		this.idle = idle;
	}
	public String getOk() {
		return ok;
	}
	public void setOk(String ok) {
		this.ok = ok;
	}
	
	public boolean isValidAll() throws ReadOnlyException {
		getHost();
		validateHost();
		
		
		if (validHost.equals(ok)) {
			validAll = true;
		} else {
			validAll = false;
		}
		
		return validAll;
	}
	public void setValidAll(boolean validAll) {
		this.validAll = validAll;
	}
    
}
