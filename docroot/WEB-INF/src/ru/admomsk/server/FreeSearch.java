/**
 * Copyright (c) 2000-2009 Liferay, Inc. All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package ru.admomsk.server;

import java.io.IOException;
import java.util.Enumeration;

import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.PortletSession;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import ru.admomsk.server.beans.EditBean;
import ru.admomsk.server.beans.ViewBean;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;



/**
 * <a href="JSPPortlet.java.html"><b><i>View Source</i></b></a>
 *
 * @author Brian Wing Shun Chan
 *
 */
public class FreeSearch extends GenericPortlet {

	protected String editJSP;
	protected String viewJSP;
	PortletURL actionUrl = null;
	
	
	private static Log _log = LogFactoryUtil.getLog(FreeSearch.class);
	
	public void init() throws PortletException {
		editJSP = getInitParameter("edit-jsp");
		viewJSP = getInitParameter("view-jsp");
	}

	public void doDispatch(
			RenderRequest renderRequest, RenderResponse renderResponse)
		throws IOException, PortletException {

		String jspPage = renderRequest.getParameter("jspPage");

		if (jspPage != null) {
			include(jspPage, renderRequest, renderResponse);
		}
		else {
			super.doDispatch(renderRequest, renderResponse);
		}
	}

	public void doEdit(
			RenderRequest renderRequest, RenderResponse renderResponse)
		throws IOException, PortletException {
		
		PortletSession portletSession = renderRequest.getPortletSession();
    	
    	if ((EditBean)portletSession.getAttribute("editBean") == null){
    		portletSession.setAttribute("editBean", new EditBean());
    	}
    	
    	//Установка настроек портлета
    	((EditBean)portletSession.getAttribute("editBean")).setPortletPreferences(renderRequest.getPreferences());
		
    	if (renderRequest.getParameter("host") != null) {
    		((EditBean)portletSession.getAttribute("editBean")).setHost(renderRequest.getParameter("host"));
    		((EditBean)portletSession.getAttribute("editBean")).validateHost();
    	}
    	
    	if (renderRequest.getParameter("host_search") != null) {
    		((EditBean)portletSession.getAttribute("editBean")).setHostSearch(renderRequest.getParameter("host_search"));
    	}
    	
		renderRequest.setAttribute("editBean", (EditBean)portletSession.getAttribute("editBean"));
		include(editJSP, renderRequest, renderResponse);
	}
	
	public void doView(
			RenderRequest renderRequest, RenderResponse renderResponse)
		throws IOException, PortletException {
		
		PortletSession portletSession = renderRequest.getPortletSession();
		
    	if ((ViewBean)portletSession.getAttribute("viewBean") == null){
    		portletSession.setAttribute("viewBean", new ViewBean());
    	}
    	
    	if ((EditBean)portletSession.getAttribute("editBean") == null){
    		portletSession.setAttribute("editBean", new EditBean());
    	}
    	
    	//Установка настроек портлета
    	((EditBean)portletSession.getAttribute("editBean")).setPortletPreferences(renderRequest.getPreferences());
    	((ViewBean)portletSession.getAttribute("viewBean")).setEditBean((EditBean)portletSession.getAttribute("editBean"));
		
    	Enumeration<String> parameterMap = renderRequest.getParameterNames();
		
		//если есть параметры, то пробуем загрузить контент, иначе пересозадим бин, тем самым очистим
		if (parameterMap.hasMoreElements()) {
			StringBuffer sbParameter = new StringBuffer();
			String nameParameter;
	    	while (parameterMap.hasMoreElements()) {
	    		nameParameter = parameterMap.nextElement();
	    		sbParameter.append(nameParameter).append("=").append(renderRequest.getParameter(nameParameter));
	    		if (parameterMap.hasMoreElements()) sbParameter.append("&");
	    	}
	    	
    		PortletURL renderURL = renderResponse.createRenderURL();
            ((ViewBean)portletSession.getAttribute("viewBean")).setTextQuery(renderRequest.getParameter("q"));
            ((ViewBean)portletSession.getAttribute("viewBean")).setResultQuery(((ViewBean)portletSession.getAttribute("viewBean")).connectMnoGoSearch(sbParameter.toString(), renderURL.toString()));
    	} else {
    		((ViewBean)portletSession.getAttribute("viewBean")).resetValue();
    	}
		
		renderRequest.setAttribute("viewBean", (ViewBean)portletSession.getAttribute("viewBean"));
		include(viewJSP, renderRequest, renderResponse);
	}

	protected void include(
			String path, RenderRequest renderRequest,
			RenderResponse renderResponse)
		throws IOException, PortletException {

		PortletRequestDispatcher portletRequestDispatcher =
			getPortletContext().getRequestDispatcher(path);

		if (portletRequestDispatcher == null) {
			_log.error(path + " is not a valid include");
		}
		else {
			portletRequestDispatcher.include(renderRequest, renderResponse);
		}
	}

}